var config = {};

var env = process.env;
if (env.OPENSHIFT_GEAR_DNS) {
    var useMemory = {
        "use_memory": true,
        "use_cache": false
    };
    var useMemoryAndCache = {
        "use_memory": true,
        "use_cache": true
    };
    // OpenShift config settings
    config = {
        server: {
            ssl: {
                use_x_forwarded: false,
                hostname: env.OPENSHIFT_GEAR_DNS,
                ensureSecureUrlPathPattern: "^/(admin|setup|user)"
            }
        },
        "siteName": "PencilBlue Website",
        // Make urls be relative to parent resource.
        "siteRoot": "http://" + env.OPENSHIFT_GEAR_DNS,
        "siteIP": env.OPENSHIFT_NODEJS_IP,
        "sitePort": env.OPENSHIFT_NODEJS_PORT,
        "logging": {
            "level": "info"
        },
        "db": {
            "type": "mongo",
            "servers": [
                env.OPENSHIFT_MONGODB_DB_URL
            ],
            "name": env.OPENSHIFT_APP_NAME,
            "writeConcern": 1
        },
        "media": {
            "parent_dir": env.OPENSHIFT_DATA_DIR
        },
        "settings": useMemory,
        "templates": useMemory,
        "plugins": {
            "caching": useMemory
        },
        "multisite": {
            "enabled": false
        }
    };
    if (env.OPENSHIFT_HAPROXY_VERSION) {
        // Scaled application
        config.cluster = {
            "self_managed": false
        };
    }
} else {
    // local dev settings
    config = {
        "siteName": "PencilBlue Website",
        "siteRoot": "http://localhost:8080",
        "siteIP": "0.0.0.0",
        "sitePort": 8080,
        "logging": {
            "level": "info"
        },
        "db": {
            "type": "mongo",
            "servers": [
                "mongodb://localhost:27017"
            ],
            "name": "pencilblue-new",
            "writeConcern": 1
        },
        // The following makes it simpler to debug
        "cluster": {
            "self_managed": false,
            "workers": 1
        },
        "multisite": {
            "enabled": false,
            "globalRoot": 'http://global.localhost:8080'
        }
    }

}

module.exports = config;
