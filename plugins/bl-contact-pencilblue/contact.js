
module.exports = function(pb) {
    
    //pb dependencies
    var util = pb.util;
    
    /**
     * Contact Form - A basic contact form plugin.
     * look like.
     *
     * @author Blake Callens <blake@pencilblue.org>
     * @copyright 2015 PencilBlue, LLC
     */
    function Contact(){}

    /**
     * Called when the application is being installed for the first time.
     *
     * @param cb A callback that must be called upon completion.  cb(Error, Boolean).
     * The result should be TRUE on success and FALSE on failure
     */
    Contact.onInstall = function(cb) {
        var self = this;
        
        var cos = new pb.CustomObjectService();
        cos.loadTypeByName('pb_contact', function(err, contactType) {
            if (util.isError(err) || contactType) {
                return cb(err, !util.isError(err));
            }
            
            var contactValues = {
                name: 'pb_contact',
                fields: {
                    name: {
                        field_type: 'text'
                    },
                    email: {
                        field_type: 'text'
                    },
                    comment: {
                        field_type: 'text'
                    },
                    date: {
                        field_type: 'date'
                    }
                }
            };

            cos.saveType(contactValues, function(err, contactType) {
                cb(err, !util.isError(err));
            });
        });
    };

    /**
     * Called when the application is uninstalling this plugin.  The plugin should
     * make every effort to clean up any plugin-specific DB items or any in function
     * overrides it makes.
     *
     * @param cb A callback that must be called upon completion.  cb(Error, Boolean).
     * The result should be TRUE on success and FALSE on failure
     */
    Contact.onUninstall = function(cb) {
      cb(null, true);
    };

    /**
     * Called when the application is starting up. The function is also called at
     * the end of a successful install. It is guaranteed that all core PB services
     * will be available including access to the core DB.
     *
     * @param cb A callback that must be called upon completion.  cb(Error, Boolean).
     * The result should be TRUE on success and FALSE on failure
     */
    Contact.onStartupWithContext = function(context, cb) {
        var self = this;
        //Get the type ID
        var typeID = "";
        var cos = new pb.CustomObjectService();
        cos.loadTypeByName('pb_contact', function(err, objectType) {
            if(objectType) {
                var site = pb.SiteService.getCurrentSite(context.site);
                pb.AdminNavigation.removeFromSite("contact_messages", site);

                if (err == null) {
                    typeID = self.stringToHex(objectType._id.id);

                    /**
                     * Administration Navigation sample
                     */
                    // Add a child to the top level node "content"
                    pb.AdminNavigation.addChildToSite("content", {
                        id: "contact_messages",
                        title: "Contact Messages",
                        icon: "inbox",
                        href: "/admin/content/objects/" + typeID,
                        access: pb.SecurityService.ACCESS_USER
                    }, site);
                }
            }
            cb(null, true);
        });
    };

    /**
     * Called when the application is gracefully shutting down.  No guarantees are
     * provided for how much time will be provided the plugin to shut down.
     *
     * @param cb A callback that must be called upon completion.  cb(Error, Boolean).
     * The result should be TRUE on success and FALSE on failure
     */
    Contact.onShutdown = function(cb) {
      cb(null, true);
    };

    Contact.stringToHex = function (tmp) {
        var str = '',
            i = 0,
            tmp_len = tmp.length,
            c;

        for (; i < tmp_len; i += 1) {
            c = tmp.charCodeAt(i);
            str += this.d2h(c);
        }
        return str;
    };
    Contact.d2h = function(number) {
        if (number < 0) {
            number = 0xFFFFFFFF + number + 1;
        }
        return this.pad(number.toString(16), 2);
    };
    Contact.pad = function(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    };

    //exports
    return Contact;
};
