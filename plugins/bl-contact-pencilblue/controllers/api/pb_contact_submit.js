/*
    Copyright (C) 2015  PencilBlue, LLC

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module.exports = function(pb) {
    
    //pb depdencies
    var util = pb.util;
    
    function ContactSubmit() {};
    util.inherits(ContactSubmit, pb.BaseController);

    ContactSubmit.prototype.render = function(cb) {
      var self = this;

      this.getJSONPostParams(function(err, post) {
        var message = self.hasRequiredParams(post, ['name', 'email']);
        if(message) {
          cb({
            code: 400,
            content: pb.BaseController.apiResponse(pb.BaseController.API_ERROR, message)
          });
          return;
        }

        var cos = new pb.CustomObjectService();
        cos.loadTypeByName('pb_contact', function(err, contactType) {
          if(util.isError(err) || !util.isObject(contactType)) {
            cb({
              code: 400,
              content: pb.BaseController.apiResponse(pb.BaseController.API_ERROR, self.ls.get('INVALID_UID'))
            });
            return;
          }

        var contactTypeId = self.stringToHex(contactType._id.id);

          var contact = {
            name: post.name + ' (' + util.uniqueId() + ')',
            email: post.email,
            description: post.email,
            comment: post.comment,
            date: new Date()
          };

          pb.CustomObjectService.formatRawForType(contact, contactType);
          var customObjectDocument = pb.DocumentCreator.create('custom_object', contact);

          cos.save(customObjectDocument, contactType, function(err, result) {
            if(util.isError(err)) {
              return cb({
                code: 500,
                content: pb.BaseController.apiResponse(pb.BaseController.API_ERROR, self.ls.get('ERROR_SAVING'))
              });
            }
            else if(util.isArray(result) && result.length > 0) {
              return cb({
                code: 500,
                content: pb.BaseController.apiResponse(pb.BaseController.API_ERROR, self.ls.get('ERROR_SAVING'))
              });
            }

              var emails = false;
              var pluginService = new pb.PluginService();
              pluginService.getSetting('send_emails', 'bl-contact-pencilblue', function(err, val) {
                  emails = val;
                  var toAddress = "";
                  pluginService.getSetting('email_to', 'bl-contact-pencilblue', function(err, val) {
                      toAddress = val;

                      if(emails == true && toAddress != "" && toAddress != null) {
                          var options = {
                              to: '"' + toAddress + '"',
                              from: post.name + "<" + toAddress + ">",
                              subject: 'New Contact Message',
                              layout: '' +
                              '<img src="http://calowbenefits.com/public/bl-calow-benefit-partners/img/logo/logo-email.png" width="300" /><br />' +
                              '<p>' +
                              'A new contact message has been submitted. The details are as follows:' +
                              '<br />' +
                              '<br />Name: ' + post.name +
                              '<br />Email: ' + post.email +
                              '<br />Message:<br />' +
                              post.comment +
                              '<br /><br /><a href="http://calowbenefits.com/admin/content/objects/' + contactTypeId + '/">View In PencilBlue</a>' +
                              '</p>'
                          };
                          var emailService = new pb.EmailService();
                          emailService.sendFromLayout(options, function(err, response) {
                              if(err) {
                                  return cb({
                                      code: 500,
                                      content: pb.BaseController.apiResponse(pb.BaseController.API_FAILURE, JSON.stringify(err))
                                  });
                              }
                              cb({content: pb.BaseController.apiResponse(pb.BaseController.API_SUCCESS, 'contact submitted; email sent;')});
                          });
                      } else {
                          cb({content: pb.BaseController.apiResponse(pb.BaseController.API_SUCCESS, 'contact submitted')});
                      }
                  });
              });
          });
        });
      });
    };

    ContactSubmit.prototype.stringToHex = function (tmp) {
        var str = '',
            i = 0,
            tmp_len = tmp.length,
            c;

        for (; i < tmp_len; i += 1) {
            c = tmp.charCodeAt(i);
            str += this.d2h(c);
        }
        return str;
    };
    ContactSubmit.prototype.d2h = function(number) {
        if (number < 0) {
            number = 0xFFFFFFFF + number + 1;
        }
        return this.pad(number.toString(16), 2);
    };
    ContactSubmit.prototype.pad = function(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    };

    ContactSubmit.getRoutes = function(cb) {
      var routes = [
        {
          method: 'post',
          path: '/api/contact/pb_contact_submit',
          auth_required: false,
          content_type: 'application/json'
        }
      ];
      cb(null, routes);
    };

    //exports
    return ContactSubmit;
};
