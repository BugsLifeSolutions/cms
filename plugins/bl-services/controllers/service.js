/*
 Copyright (C) 2015  PencilBlue, LLC

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//dependencies
var path  = require('path');
var async = require('async');
var _     = require('underscore');

module.exports = function ServicesObjects(pb) {

    //pb dependencies
    var util           = pb.util;
    var config         = pb.config;
    var TopMenu        = pb.TopMenuService;
    var Comments       = pb.CommentService;
    var ArticleService = pb.ArticleService;

    /**
     * Index page of the services plugin
     * @deprecated Since 0.4.1
     * @class Index
     * @constructor
     * @extends BaseController
     */
    function ServicesObjects(){}
    util.inherits(ServicesObjects, pb.BaseController);

    ServicesObjects.prototype.render = function(cb) {
        var self = this;
        var serviceName = this.pathVars.service_name;

        var contentService = new pb.ContentService();
        contentService.getSettings(function(err, contentSettings) {
            self.gatherData(function(err, data) {

                var articleService = new pb.ArticleService();
                articleService.getMetaInfo(data.content[0], function(err, meta) {
                    self.ts.registerLocal('navigation', new pb.TemplateValue(data.nav.navigation, false));
                });
            });
        });

        // Custom Services Item
        var daoServicesItem = new pb.DAO();
        daoServicesItem.q('custom_object', {where: {name: serviceName}, limit: 1}, function(err, servicesItem) {
            if (util.isError(servicesItem)) {
                return self.reqHandler.serveError(err);
            }
            //none to display
            if(servicesItem.length == 0) {
                return self.reqHandler.serve404();
            } else {
                var daoMediaObjects = new pb.DAO();
                var mediaObjectResults = [];
                _.each(servicesItem[0].Media, function(mediaId) {
                    daoMediaObjects.loadById(mediaId, 'media', function(err, mediaResults) {
                        if (util.isError(mediaResults)) {
                            return self.reqHandler.serveError(err);
                        }
                        mediaObjectResults.push(mediaResults);
                        var mediaObjects = pb.ClientJs.getAngularObjects({
                            mediaObjects: mediaObjectResults
                        });
                        self.ts.registerLocal('media_objects', new pb.TemplateValue(mediaObjects, false));
                    });
                });

                var angularObjects = pb.ClientJs.getAngularObjects({
                    customObject: servicesItem[0]
                });
                self.ts.registerLocal('angular_objects', new pb.TemplateValue(angularObjects, false));

                var mediaObjects = pb.ClientJs.getAngularObjects({
                    mediaObjects: mediaObjectResults
                });
                self.ts.registerLocal('media_objects', new pb.TemplateValue(mediaObjects, false));

                self.ts.registerLocal('page_name', new pb.TemplateValue(servicesItem[0]['Display Name'] + ' | ' + config.siteName, false));

                self.ts.registerLocal('service_display_name', new pb.TemplateValue(servicesItem[0]['Display Name'], false));

                self.ts.load('service', function(err, result) {
                    cb({content: result});
                });
            }
        });
    };

    ServicesObjects.prototype.gatherData = function(cb) {
        var self  = this;
        var tasks = {

            //navigation
            nav: function(callback) {
                self.getNavigation(function(themeSettings, navigation, accountButtons) {
                    callback(null, {themeSettings: themeSettings, navigation: navigation, accountButtons: accountButtons});
                });
            },

            //articles, pages, etc.
            content: function(callback) {
                self.loadContent(callback);
            },

            section: function(callback) {
                if(!self.req.pencilblue_section) {
                    callback(null, {});
                    return;
                }

                var dao = new pb.DAO();
                dao.loadById(self.req.pencilblue_section, 'section', callback);
            }
        };
        async.parallel(tasks, cb);
    };

    ServicesObjects.prototype.loadContent = function(articleCallback) {

        var section = this.req.pencilblue_section || null;
        var topic   = this.req.pencilblue_topic   || null;
        var article = this.req.pencilblue_article || null;
        var page    = this.req.pencilblue_page    || null;

        //get service context
        var opts = this.getServiceContext();

        var service = new ArticleService();
        if(this.req.pencilblue_preview) {
            if(this.req.pencilblue_preview == page || article) {
                if(page) {
                    service.setContentType('page');
                }
                var where = pb.DAO.getIdWhere(page || article);
                where.draft = {$exists: true};
                where.publish_date = {$exists: true};
                service.find(where, opts, articleCallback);
            }
            else {
                service.find({}, opts, articleCallback);
            }
        }
        else if(section) {
            service.findBySection(section, articleCallback);
        }
        else if(topic) {
            service.findByTopic(topic, articleCallback);
        }
        else if(article) {
            service.findById(article, articleCallback);
        }
        else if(page) {
            service.setContentType('page');
            service.findById(page, articleCallback);
        }
        else{
            service.find({}, opts, articleCallback);
        }
    };

    ServicesObjects.prototype.getNavigation = function(cb) {
        var options = {
            currUrl: this.req.url,
            session: this.session,
            ls: this.ls,
            activeTheme: this.activeTheme
        };

        var menuService = new pb.TopMenuService();
        menuService.getNavItems(options, function(err, navItems) {
            if (util.isError(err)) {
                pb.log.error('ServicesObjects: %s', err.stack);
            }
            cb(navItems.themeSettings, navItems.navigation, navItems.accountButtons);
        });
    };

    /**
     * Provides the routes that are to be handled by an instance of this prototype.
     * The route provides a definition of path, permissions, authentication, and
     * expected content type.
     * Method is optional
     * Path is required
     * Permissions are optional
     * Access levels are optional
     * Content type is optional
     *
     * @param cb A callback of the form: cb(error, array of objects)
     */
    ServicesObjects.getRoutes = function(cb) {
        var routes = [
            {
                method: 'get',
                path: '/page/service/:service_name',
                auth_required: false,
                content_type: 'text/html'
            }
        ];
        cb(null, routes);
    };

    //exports
    return ServicesObjects;
};