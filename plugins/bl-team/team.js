/**
 * Created by HadenHiles on 15-09-11.
 */

module.exports = function TeamModule(pb) {

    /**
     * Team - A plugin for PencilBlue used to display team members
     *
     * @author Blake Callens <blake@pencilblue.org>
     * @copyright 2014 PencilBlue, LLC
     */
    function Team(){}

    /**
     * Called when the application is being installed for the first time.
     *
     * @param cb A callback that must be called upon completion.  cb(err, result).
     * The result is ignored
     */
    Team.onInstall = function(cb) {
        var self = this;

        var cos = new pb.CustomObjectService();
        cos.loadTypeByName('Team', function(err, teamType) {
            if (!teamType) {
                var teamValues = {
                    name: 'Team',
                    fields: {
                        name: {
                            field_type: 'text'
                        },
                        "Display Name": {
                            field_type: 'text'
                        },
                        "Position": {
                            field_type: 'text'
                        },
                        Details: {
                            field_type: 'wysiwyg'
                        },
                        Photo: {
                            field_type: 'peer_object',
                            object_type: 'media'
                        }
                    }
                };

                cos.saveType(teamValues, function (err, teamType) {
                    cb(err, true);
                });
            } else {
                cb(null, true);
            }
        });
    };

    /**
     * Called when the application is uninstalling this plugin.  The plugin should
     * make every effort to clean up any plugin-specific DB items or any in function
     * overrides it makes.
     *
     * @param cb A callback that must be called upon completion.  cb(err, result).
     * The result is ignored
     */
    Team.onUninstall = function(cb) {
        cb(null, true);
    };

    /**
     * Called when the application is starting up. The function is also called at
     * the end of a successful install. It is guaranteed that all core PB services
     * will be available including access to the core DB.
     *
     * @param cb A callback that must be called upon completion.  cb(err, result).
     * The result is ignored
     */
    Team.onStartupWithContext = function(context, cb) {
        var self = this;
        //Get the type ID
        var typeID = "";
        var cos = new pb.CustomObjectService();
        cos.loadTypeByName('Team', function(err, objectType) {
            if(objectType) {
                var site = pb.SiteService.getCurrentSite(context.site);
                pb.AdminNavigation.removeFromSite("team", site);

                if (err == null) {
                    typeID = self.stringToHex(objectType._id.id);

                    /**
                     * Administration Navigation sample
                     */
                    // Add a child to the top level node "content"
                    pb.AdminNavigation.addChildToSite("content", {
                        id: "team",
                        title: "Our Team",
                        icon: "users",
                        href: "/admin/content/objects/" + typeID,
                        access: pb.SecurityService.ACCESS_USER
                    }, site);
                }
            }
            cb(null, true);
        });
    };

    /**
     * Called when the application is gracefully shutting down.  No guarantees are
     * provided for how much time will be provided the plugin to shut down.
     *
     * @param cb A callback that must be called upon completion.  cb(err, result).
     * The result is ignored
     */
    Team.onShutdown = function(cb) {
        cb(null, true);
    };

    Team.stringToHex = function (tmp) {
        var str = '',
            i = 0,
            tmp_len = tmp.length,
            c;

        for (; i < tmp_len; i += 1) {
            c = tmp.charCodeAt(i);
            str += this.d2h(c);
        }
        return str;
    };
    Team.d2h = function(number) {
        if (number < 0) {
            number = 0xFFFFFFFF + number + 1;
        }
        return this.pad(number.toString(16), 2);
    };
    Team.pad = function(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    };

    //exports
    return Team;
};
