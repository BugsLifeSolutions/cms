/*
 Copyright (C) 2015  PencilBlue, LLC

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//dependencies
var path  = require('path');
var async = require('async');

module.exports = function IndexModule(pb) {

    //pb dependencies
    var util           = pb.util;

    /**
     * Index page of the pencilblue theme
     * @deprecated Since 0.4.1
     * @class Index
     * @constructor
     * @extends BaseController
     */
    function Index(){}
    util.inherits(Index, pb.BaseController);

    /**
     * A sample redirect endpoint
     * @method redirectToHomepage
     * @params {Function} cb
     */
    Index.prototype.redirectToHome = function(cb) {
        this.redirect('/page/home', cb);
    };

    /**
     * Provides the routes that are to be handled by an instance of this prototype.
     * The route provides a definition of path, permissions, authentication, and
     * expected content type.
     * Method is optional
     * Path is required
     * Permissions are optional
     * Access levels are optional
     * Content type is optional
     *
     * @param cb A callback of the form: cb(error, array of objects)
     */
    Index.getRoutes = function(cb) {
        var routes = [
            {
                method: 'get',
                path: '/',
                handler: 'redirectToHome'
            }
        ];
        cb(null, routes);
    };

    //exports
    return Index;
};

