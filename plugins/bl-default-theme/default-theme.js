/**
 * Created by HadenHiles on 15-09-11.
 */

module.exports = function DemoModule(pb) {

    /**
     * Demo - A Demo site theme for PencilBlue
     *
     * @author Blake Callens <blake@pencilblue.org>
     * @copyright 2014 PencilBlue, LLC
     */
    function Demo(){}

    /**
     * Called when the application is being installed for the first time.
     *
     * @param cb A callback that must be called upon completion.  cb(err, result).
     * The result is ignored
     */
    Demo.onInstall = function(cb) {
        cb(null, true);
    };

    /**
     * Called when the application is uninstalling this plugin.  The plugin should
     * make every effort to clean up any plugin-specific DB items or any in function
     * overrides it makes.
     *
     * @param cb A callback that must be called upon completion.  cb(err, result).
     * The result is ignored
     */
    Demo.onUninstall = function(cb) {
        cb(null, true);
    };

    /**
     * Called when the application is starting up. The function is also called at
     * the end of a successful install. It is guaranteed that all core PB services
     * will be available including access to the core DB.
     *
     * @param cb A callback that must be called upon completion.  cb(err, result).
     * The result is ignored
     */
    Demo.onStartup = function(cb) {
        pb.TemplateService.registerGlobal('site_slogan', function(flag, cb) {
            var pluginService = new pb.PluginService();
            pluginService.getSetting('site_slogan', 'bl-default-theme', cb);
        });
        pb.TemplateService.registerGlobal('primary_location', function(flag, cb) {
            var pluginService = new pb.PluginService();
            pluginService.getSetting('primary_location', 'bl-default-theme', cb);
        });
        pb.TemplateService.registerGlobal('primary_phone', function(flag, cb) {
            var pluginService = new pb.PluginService();
            pluginService.getSetting('primary_phone', 'bl-default-theme', cb);
        });
        pb.TemplateService.registerGlobal('primary_email', function(flag, cb) {
            var pluginService = new pb.PluginService();
            pluginService.getSetting('primary_email', 'bl-default-theme', cb);
        });
        pb.TemplateService.registerGlobal('facebook_page', function(flag, cb) {
            var pluginService = new pb.PluginService();
            pluginService.getSetting('facebook_page', 'bl-default-theme', cb);
        });

        //Plugin Path
        pb.TemplateService.registerGlobal('theme_path', function(flag, cb) {
            var pluginService = new pb.PluginService();
            pluginService.getSetting('theme_path', 'bl-default-theme', cb);
        });

        cb(null, true);
    };

    /**
     * Called when the application is gracefully shutting down.  No guarantees are
     * provided for how much time will be provided the plugin to shut down.
     *
     * @param cb A callback that must be called upon completion.  cb(err, result).
     * The result is ignored
     */
    Demo.onShutdown = function(cb) {
        cb(null, true);
    };

    //exports
    return Demo;
};
