/**
 * Created by HadenHiles on 15-10-20.
 */
$(document).ready(function() {
    $("p.page_break").first().removeClass("page_break");

    //Move the breadcrumb to the navbar so that it flows underneath dropdown on mobile devices
    var $breadcrumb = $(".breadcrumb");
    if($breadcrumb.length > 0) {
        $breadcrumb.appendTo($(".navbar .container"));
    }

    //Modify all phone links to exclude special characters
    var $phoneLink = $('a.phone');
    $phoneLink.each(function() {
        var validPhoneLink = $(this).attr("href").toLowerCase().replace(/[-()]/g, '').replace(/[_\s]/g, '');
        $(this).attr("href", validPhoneLink);
    });

    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function() {
        $('.navbar-toggle:visible').click();
    });
});