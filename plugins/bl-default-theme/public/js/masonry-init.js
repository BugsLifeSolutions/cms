/**
 * Created by HadenHiles on 15-09-24.
 */
$(document).ready(function() {
    // with jQuery
    var $grid = $('.grid');

// initialize Masonry after all images have loaded
    $grid.imagesLoaded(function() {
        $grid.masonry({
            // options...
            itemSelector: '.grid-item',
            columnWidth: '.grid-sizer',
            gutter: 9,
            percentPosition: true,
            isAnimated: true,
            isFitWidth: true
        });
    });
});