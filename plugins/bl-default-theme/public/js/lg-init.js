/**
 * Created by HadenHiles on 15-09-22.
 */
$(document).ready(function() {
    var $lg = $("#lightgallery");
    $lg.lightGallery({
        selector: '.gallery_item',
        mousewheel: false,
        download: false
    });

    $lg.on('onAfterOpen.lg', function(event){
        $('.lg-toogle-thumb').trigger('click');
    });
});