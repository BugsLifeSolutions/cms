/**
 * Created by HadenHiles on 15-09-11.
 */

module.exports = function PortfolioModule(pb) {

    /**
     * Portfolio - A Portfolio plugin for PencilBlue
     *
     * @author Blake Callens <blake@pencilblue.org>
     * @copyright 2014 PencilBlue, LLC
     */
    function Portfolio(){}

    /**
     * Called when the application is being installed for the first time.
     *
     * @param cb A callback that must be called upon completion.  cb(err, result).
     * The result is ignored
     */
    Portfolio.onInstall = function(cb) {
        var self = this;

        var cos = new pb.CustomObjectService();
        cos.loadTypeByName('Portfolio', function(err, portfolioType) {
            if (!portfolioType) {
                var portfolioValues = {
                    name: 'Portfolio',
                    fields: {
                        name: {
                            field_type: 'text'
                        },
                        "Display Name": {
                            field_type: 'text'
                        },
                        "Date Completed": {
                            field_type: 'date'
                        },
                        Content: {
                            field_type: 'wysiwyg'
                        },
                        Media: {
                            field_type: 'child_objects',
                            object_type: 'media'
                        }
                    }
                };

                cos.saveType(portfolioValues, function (err, portfolioType) {
                    cb(err, true);
                });
            } else {
                cb(null, true);
            }
        });
    };

    /**
     * Called when the application is uninstalling this plugin.  The plugin should
     * make every effort to clean up any plugin-specific DB items or any in function
     * overrides it makes.
     *
     * @param cb A callback that must be called upon completion.  cb(err, result).
     * The result is ignored
     */
    Portfolio.onUninstall = function(cb) {
        cb(null, true);
    };

    /**
     * Called when the application is starting up. The function is also called at
     * the end of a successful install. It is guaranteed that all core PB services
     * will be available including access to the core DB.
     *
     * @param cb A callback that must be called upon completion.  cb(err, result).
     * The result is ignored
     */
    Portfolio.onStartupWithContext = function(context, cb) {
        var self = this;
        //Get the type ID
        var typeID = "";
        var cos = new pb.CustomObjectService();
        cos.loadTypeByName('Portfolio', function(err, objectType) {
            if(objectType) {
                var site = pb.SiteService.getCurrentSite(context.site);
                pb.AdminNavigation.removeFromSite("portfolio", site);

                if (err == null) {
                    typeID = self.stringToHex(objectType._id.id);

                    /**
                     * Administration Navigation sample
                     */
                    // Add a child to the top level node "content"
                    pb.AdminNavigation.addChildToSite("content", {
                        id: "portfolio",
                        title: "Portfolio",
                        icon: "folder-open",
                        href: "/admin/content/objects/" + typeID,
                        access: pb.SecurityService.ACCESS_USER
                    }, site);
                }
            }
            cb(null, true);
        });
    };

    /**
     * Called when the application is gracefully shutting down.  No guarantees are
     * provided for how much time will be provided the plugin to shut down.
     *
     * @param cb A callback that must be called upon completion.  cb(err, result).
     * The result is ignored
     */
    Portfolio.onShutdown = function(cb) {
        cb(null, true);
    };

    Portfolio.stringToHex = function (tmp) {
        var str = '',
            i = 0,
            tmp_len = tmp.length,
            c;

        for (; i < tmp_len; i += 1) {
            c = tmp.charCodeAt(i);
            str += this.d2h(c);
        }
        return str;
    };
    Portfolio.d2h = function(number) {
        if (number < 0) {
            number = 0xFFFFFFFF + number + 1;
        }
        return this.pad(number.toString(16), 2);
    };
    Portfolio.pad = function(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    };

    //exports
    return Portfolio;
};
