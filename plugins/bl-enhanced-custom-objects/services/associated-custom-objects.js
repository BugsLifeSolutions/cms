var _ = require('underscore');

module.exports = function(pb) {
    // Initialize the service object
    function AssociatedCustomObjects() {}

    // This function will be called when PencilBlue loads the service
    AssociatedCustomObjects.init = function(cb) {
        pb.log.debug("Associated Custom Objects: Initialized");
        cb(null, true);
    };

    // Return the quote object and it's employees
    AssociatedCustomObjects.prototype.associated = function(id, property, excludedAssociatedProps, cb) {
        var primaryObjectId = id;
        var key = property;
        var excluded = excludedAssociatedProps;

        var associated = [];
        var dao = new pb.DAO();
        dao.loadById(primaryObjectId, 'custom_object', {}, function(err, result) {
            if (err != null) {
                return self.reqHandler.serveError(err);
            }

            //dao.q('custom_object', {where: {_id: {$in: result.Employees}}}, cb);
            _.each(result[key], function (objectID) {
                dao.loadById(objectID, 'custom_object', {}, function (err, customObject) {
                    if (err != null) {
                        return self.reqHandler.serveError(err);
                    }

                    _.each(excluded, function(excludeKey) {
                        delete customObject[excludeKey];
                    });

                    associated.push(customObject);

                    if(associated.length == result[key].length) {
                        cb(null, associated);
                    }
                });
            });
        });
    };

    return AssociatedCustomObjects;
};
