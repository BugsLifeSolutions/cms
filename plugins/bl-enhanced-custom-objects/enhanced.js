
module.exports = function EnhancedModule(pb) {

    //pb dependencies
    var util = pb.util;

    /**
     * @class Enhanced
     * @constructor
     */
    function Enhanced(){}

    /**
     * Called when the application is being installed for the first time.
     *
     * @param cb A callback that must be called upon completion.  cb(err, result).
     * The result is ignored
     */
    Enhanced.onInstall = function(cb) {
        cb(null, true);
    };

    /**
     * Called when the application is uninstalling this plugin.  The plugin should
     * make every effort to clean up any plugin-specific DB items or any in function
     * overrides it makes.
     *
     * @param cb A callback that must be called upon completion.  cb(err, result).
     * The result is ignored
     */
    Enhanced.onUninstall = function(cb) {
        cb(null, true);
    };

    /**
     * Called when the application is starting up. The function is also called at
     * the end of a successful install. It is guaranteed that all core PB services
     * will be available including access to the core DB.
     *
     * @param cb A callback that must be called upon completion.  cb(err, result).
     * The result is ignored
     */
    Enhanced.onStartup = function(cb) {
        cb(null, true);
    };

    /**
     * Called on each request
     *
     */
    Enhanced.onRequest = function(req, session, ls, cb) {
        cb(null, true);
    };

    /**
     * Called when the application is gracefully shutting down.  No guarantees are
     * provided for how much time will be provided the plugin to shut down.
     *
     * @param cb A callback that must be called upon completion.  cb(err, result).
     * The result is ignored
     */
    Enhanced.onShutdown = function(cb) {
        cb(null, true);
    };

    //exports
    return Enhanced;
};
