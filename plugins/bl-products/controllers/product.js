/*
 Copyright (C) 2015  PencilBlue, LLC

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//dependencies
var path  = require('path');
var async = require('async');
var _     = require('underscore');

module.exports = function PortfolioObjects(pb) {

    //pb dependencies
    var util           = pb.util;
    var config         = pb.config;
    var TopMenu        = pb.TopMenuService;
    var Comments       = pb.CommentService;
    var ArticleService = pb.ArticleService;

    /**
     * Index page of the portfolio plugin
     * @deprecated Since 0.4.1
     * @class Index
     * @constructor
     * @extends BaseController
     */
    function PortfolioObjects(){}
    util.inherits(PortfolioObjects, pb.BaseController);

    PortfolioObjects.prototype.render = function(cb) {
        var self = this;
        var sectionName = this.pathVars.section_name;
        self.ts.registerLocal('section_name', new pb.TemplateValue(sectionName, false));
        var categoryName = this.pathVars.category_name;
        self.ts.registerLocal('category_name', new pb.TemplateValue(categoryName, false));
        var productName = this.pathVars.product_name;
        self.ts.registerLocal('product_name', new pb.TemplateValue(productName, false));

        //Always load a section_display_name and category_display_name
        var angularSectionObject = pb.ClientJs.getAngularObjects({
            sectionObject: {}
        });
        self.ts.registerLocal('section_object', new pb.TemplateValue(angularSectionObject, false));
        var angularCategoryObject = pb.ClientJs.getAngularObjects({
            categoryObject: {}
        });
        self.ts.registerLocal('category_object', new pb.TemplateValue(angularCategoryObject, false));

        if(sectionName != null && sectionName != '') {
            // Get the Product Section Display Name
            var daoSection = new pb.DAO();
            daoSection.q('custom_object', {
                where: {name: sectionName},
                limit: 1
            }, function (err, sectionObject) {
                if (util.isError(err)) {
                    return self.serveError(err);
                } else if (sectionObject.length == 0) {
                    //return self.reqHandler.serve404();
                } else {
                    var angularSectionObject = pb.ClientJs.getAngularObjects({
                        sectionObject: sectionObject[0]
                    });
                    self.ts.registerLocal('section_object', new pb.TemplateValue(angularSectionObject, false));
                }
            });

            if(categoryName != null && categoryName != '') {
                // Get the Product Category Display Name
                var daoCategory = new pb.DAO();
                daoCategory.q('custom_object', {where: {name: categoryName}, limit: 1}, function(err, categoryObject) {
                    if (util.isError(err)) {
                        return self.serveError(err);
                    } else if (!util.isObject(categoryObject[0])) {
                        //return self.reqHandler.serve404();
                    } else {
                        var angularCategoryObject = pb.ClientJs.getAngularObjects({
                            categoryObject: categoryObject[0]
                        });
                        self.ts.registerLocal('category_object', new pb.TemplateValue(angularCategoryObject, false));
                    }
                });
            }
        }

        var contentService = new pb.ContentService();
        contentService.getSettings(function(err, contentSettings) {
            self.gatherData(function(err, data) {

                var articleService = new pb.ArticleService();
                articleService.getMetaInfo(data.content[0], function(err, meta) {
                    self.ts.registerLocal('navigation', new pb.TemplateValue(data.nav.navigation, false));
                });
            });
        });

        // Custom Product
        var daoProductItem = new pb.DAO();
        daoProductItem.q('custom_object', {where: {name: productName}, limit: 1}, function(err, productObject) {
            if (util.isError(productObject[0])) {
                return self.reqHandler.serveError(err);
                //return self.reqHandler.serve404();
            }
            //none to display
            if(productObject.length == 0) {
                return self.reqHandler.serve404();
            } else {
                var daoMediaObjects = new pb.DAO();
                var mediaObjectResults = [];
                _.each(productObject[0].Media, function(mediaId) {
                    daoMediaObjects.loadById(mediaId, 'media', {}, function(err, mediaResults) {
                        if (util.isError(mediaResults)) {
                            return self.reqHandler.serveError(err);
                        }
                        mediaObjectResults.push(mediaResults);

                        var mediaObjects = pb.ClientJs.getAngularObjects({
                            mediaObjects: mediaObjectResults
                        });
                        self.ts.registerLocal('media_objects', new pb.TemplateValue(mediaObjects, false));
                    });
                });

                var angularObjects = pb.ClientJs.getAngularObjects({
                    customObject: productObject[0]
                });
                self.ts.registerLocal('angular_objects', new pb.TemplateValue(angularObjects, false));

                var mediaObjects = pb.ClientJs.getAngularObjects({
                    mediaObjects: mediaObjectResults
                });
                self.ts.registerLocal('media_objects', new pb.TemplateValue(mediaObjects, false));

                //Load the content for the page
                self.ts.registerLocal('page_name', new pb.TemplateValue(productObject[0]['Display Name'] + ' | ' + config.siteName, false));

                self.ts.load('product', function(err, result) {
                    cb({content: result});
                });
            }
        });
    };

    PortfolioObjects.prototype.gatherData = function(cb) {
        var self  = this;
        var tasks = {

            //navigation
            nav: function(callback) {
                self.getNavigation(function(themeSettings, navigation, accountButtons) {
                    callback(null, {themeSettings: themeSettings, navigation: navigation, accountButtons: accountButtons});
                });
            },

            //articles, pages, etc.
            content: function(callback) {
                self.loadContent(callback);
            },

            section: function(callback) {
                if(!self.req.pencilblue_section) {
                    callback(null, {});
                    return;
                }

                var dao = new pb.DAO();
                dao.loadById(self.req.pencilblue_section, {}, 'section', callback);
            }
        };
        async.parallel(tasks, cb);
    };

    PortfolioObjects.prototype.loadContent = function(articleCallback) {

        var section = this.req.pencilblue_section || null;
        var topic   = this.req.pencilblue_topic   || null;
        var article = this.req.pencilblue_article || null;
        var page    = this.req.pencilblue_page    || null;

        //get service context
        var opts = this.getServiceContext();

        var service = new ArticleService();
        if(this.req.pencilblue_preview) {
            if(this.req.pencilblue_preview == page || article) {
                if(page) {
                    service.setContentType('page');
                }
                var where = pb.DAO.getIdWhere(page || article);
                where.draft = {$exists: true};
                where.publish_date = {$exists: true};
                service.find(where, opts, articleCallback);
            }
            else {
                service.find({}, opts, articleCallback);
            }
        }
        else if(section) {
            service.findBySection(section, articleCallback);
        }
        else if(topic) {
            service.findByTopic(topic, articleCallback);
        }
        else if(article) {
            service.findById(article, articleCallback);
        }
        else if(page) {
            service.setContentType('page');
            service.findById(page, articleCallback);
        }
        else{
            service.find({}, opts, articleCallback);
        }
    };

    PortfolioObjects.prototype.getNavigation = function(cb) {
        var options = {
            currUrl: this.req.url,
            session: this.session,
            ls: this.ls,
            activeTheme: this.activeTheme
        };

        var menuService = new pb.TopMenuService();
        menuService.getNavItems(options, function(err, navItems) {
            if (util.isError(err)) {
                pb.log.error('PortfolioObjects: %s', err.stack);
            }
            cb(navItems.themeSettings, navItems.navigation, navItems.accountButtons);
        });
    };

    /**
     * Provides the routes that are to be handled by an instance of this prototype.
     * The route provides a definition of path, permissions, authentication, and
     * expected content type.
     * Method is optional
     * Path is required
     * Permissions are optional
     * Access levels are optional
     * Content type is optional
     *
     * @param cb A callback of the form: cb(error, array of objects)
     */
    PortfolioObjects.getRoutes = function(cb) {
        var routes = [
            {
                method: 'get',
                path: '/page/products/:section_name/:category_name/:product_name',
                auth_required: false,
                content_type: 'text/html'
            },
            {
                method: 'get',
                path: '/page/product/:product_name',
                auth_required: false,
                content_type: 'text/html'
            }
        ];
        cb(null, routes);
    };

    //exports
    return PortfolioObjects;
};