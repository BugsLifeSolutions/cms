/*
 Copyright (C) 2015  PencilBlue, LLC

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//dependencies
var path  = require('path');
var async = require('async');
var _     = require('underscore');

module.exports = function SectionObjects(pb) {

    //pb dependencies
    var util           = pb.util;
    var config         = pb.config;
    var TopMenu        = pb.TopMenuService;
    var Comments       = pb.CommentService;
    var ArticleService = pb.ArticleService;

    /**
     * Index page of the portfolio plugin
     * @deprecated Since 0.4.1
     * @class Index
     * @constructor
     * @extends BaseController
     */
    function SectionObjects(){}
    util.inherits(SectionObjects, pb.BaseController);

    SectionObjects.prototype.render = function(cb) {
        var self = this;

        var contentService = new pb.ContentService();
        contentService.getSettings(function(err, contentSettings) {
            self.gatherData(function(err, data) {

                var articleService = new pb.ArticleService();
                articleService.getMetaInfo(data.content[0], function(err, meta) {
                    self.ts.registerLocal('navigation', new pb.TemplateValue(data.nav.navigation, false));
                });
            });
        });

        //Load the content for the page
        var daoContent = new pb.DAO();
        daoContent.q('page', {where: {url: 'products'}, limit: 1}, function(err, pageObject) {
            if (util.isError(err)) {
                return self.serveError(err);
            }
            else if (!util.isObject(pageObject)) {
                return self.reqHandler.serve404();
            }
            self.ts.registerLocal('page_headline', new pb.TemplateValue(pageObject[0].headline, false));
            self.ts.registerLocal('page_content', new pb.TemplateValue(pageObject[0].page_layout, false));
        });

        // Custom Product Sections
        var opts = {
            where: {name: 'Product Sections'},
            limit: 1
        };
        var daoSectionType = new pb.DAO();
        daoSectionType.q('custom_object_type', opts, function(err, sectionObjectType) {
            if (util.isError(err)) {
                return self.serveError(err);
            }
            else if (!util.isObject(sectionObjectType)) {
                return self.reqHandler.serve404();
            }
            var sectionTypeObjectId = self.stringToHex(sectionObjectType[0]._id.id);

            var opts1 = {
                where: {type: sectionTypeObjectId},
                order: {'Display Name': 1}
            };
            var daoSections = new pb.DAO();
            daoSections.q('custom_object', opts1, function(err, sectionObjects) {
                if (util.isError(sectionObjects)) {
                    return self.reqHandler.serveError(err);
                }

                _.each(sectionObjects, function(sectionObject) {
                    var imageUrl = '/public/bl-default-theme/img/placeholders/placeholder.png';
                    var imageName = 'Placeholder';
                    _.each(sectionObject.Media, function(mediaId) {
                        var daoSectionMedia = new pb.DAO();
                        daoSectionMedia.loadById(mediaId, 'media', {}, function(err, mediaObject) {
                            if(util.isError(mediaObject)) {
                                return self.reqHandler.serveError(err);
                            }
                            if(mediaObject !== null && mediaObject.media_type == 'image') {
                                imageUrl = mediaObject.location;
                                imageName = mediaObject.name;
                            }

                            sectionObject['imageUrl'] = imageUrl;
                            sectionObject['imageName'] = imageName;

                            //Register the angular array of section objects
                            var angularObjects = pb.ClientJs.getAngularObjects({ sectionObjects: sectionObjects });
                            self.ts.registerLocal('section_objects', new pb.TemplateValue(angularObjects, false));
                        });
                    });
                    sectionObject['imageUrl'] = imageUrl;
                    sectionObject['imageName'] = imageName;
                });

                //Register the angular array of section objects
                var angularObjects = pb.ClientJs.getAngularObjects({ sectionObjects: sectionObjects });
                self.ts.registerLocal('section_objects', new pb.TemplateValue(angularObjects, false));

                self.ts.registerLocal('page_name', new pb.TemplateValue('Products | ' + config.siteName, false));
                self.ts.load('sections', function(err, result) {
                    if (util.isError(err)) {
                        return cb(err);
                    }
                    cb({content: result});
                });
            });
        });
    };

    SectionObjects.prototype.gatherData = function(cb) {
        var self  = this;
        var tasks = {

            //navigation
            nav: function(callback) {
                self.getNavigation(function(themeSettings, navigation, accountButtons) {
                    callback(null, {themeSettings: themeSettings, navigation: navigation, accountButtons: accountButtons});
                });
            },

            //articles, pages, etc.
            content: function(callback) {
                self.loadContent(callback);
            },

            section: function(callback) {
                if(!self.req.pencilblue_section) {
                    callback(null, {});
                    return;
                }

                var dao = new pb.DAO();
                dao.loadById(self.req.pencilblue_section, {}, 'section', callback);
            }
        };
        async.parallel(tasks, cb);
    };

    SectionObjects.prototype.loadContent = function(articleCallback) {

        var section = this.req.pencilblue_section || null;
        var topic   = this.req.pencilblue_topic   || null;
        var article = this.req.pencilblue_article || null;
        var page    = this.req.pencilblue_page    || null;

        //get service context
        var opts = this.getServiceContext();

        var service = new ArticleService();
        if(this.req.pencilblue_preview) {
            if(this.req.pencilblue_preview == page || article) {
                if(page) {
                    service.setContentType('page');
                }
                var where = pb.DAO.getIdWhere(page || article);
                where.draft = {$exists: true};
                where.publish_date = {$exists: true};
                service.find(where, opts, articleCallback);
            }
            else {
                service.find({}, opts, articleCallback);
            }
        }
        else if(section) {
            service.findBySection(section, articleCallback);
        }
        else if(topic) {
            service.findByTopic(topic, articleCallback);
        }
        else if(article) {
            service.findById(article, articleCallback);
        }
        else if(page) {
            service.setContentType('page');
            service.findById(page, articleCallback);
        }
        else{
            service.find({}, opts, articleCallback);
        }
    };

    SectionObjects.prototype.getNavigation = function(cb) {
        var options = {
            currUrl: this.req.url,
            session: this.session,
            ls: this.ls,
            activeTheme: this.activeTheme
        };

        var menuService = new pb.TopMenuService();
        menuService.getNavItems(options, function(err, navItems) {
            if (util.isError(err)) {
                pb.log.error('SectionObjects: %s', err.stack);
            }
            cb(navItems.themeSettings, navItems.navigation, navItems.accountButtons);
        });
    };

    SectionObjects.prototype.stringToHex = function (tmp) {
        var str = '',
            i = 0,
            tmp_len = tmp.length,
            c;

        for (; i < tmp_len; i += 1) {
            c = tmp.charCodeAt(i);
            str += this.d2h(c);
        }
        return str;
    };
    SectionObjects.prototype.d2h = function(number) {
        if (number < 0) {
            number = 0xFFFFFFFF + number + 1;
        }
        return this.pad(number.toString(16), 2);
    };
    SectionObjects.prototype.pad = function(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    };

    /**
     * Provides the routes that are to be handled by an instance of this prototype.
     * The route provides a definition of path, permissions, authentication, and
     * expected content type.
     * Method is optional
     * Path is required
     * Permissions are optional
     * Access levels are optional
     * Content type is optional
     *
     * @param cb A callback of the form: cb(error, array of objects)
     */
    SectionObjects.getRoutes = function(cb) {
        var routes = [
            {
                method: 'get',
                path: '/page/products',
                auth_required: false,
                content_type: 'text/html'
            }
        ];
        cb(null, routes);
    };

    //exports
    return SectionObjects;
};