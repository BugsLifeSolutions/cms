/*
 Copyright (C) 2015  PencilBlue, LLC

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//dependencies
var path  = require('path');
var async = require('async');
var _     = require('underscore');

module.exports = function CategoryObjects(pb) {

    //pb dependencies
    var util           = pb.util;
    var config         = pb.config;
    var TopMenu        = pb.TopMenuService;
    var Comments       = pb.CommentService;
    var ArticleService = pb.ArticleService;

    /**
     * Index page of the portfolio plugin
     * @deprecated Since 0.4.1
     * @class Index
     * @constructor
     * @extends BaseController
     */
    function CategoryObjects(){}
    util.inherits(CategoryObjects, pb.BaseController);

    CategoryObjects.prototype.render = function(cb) {
        var self = this;
        var sectionName = this.pathVars.section_name;
        self.ts.registerLocal('section_name', new pb.TemplateValue(sectionName, false));

        var contentService = new pb.ContentService();
        contentService.getSettings(function(err, contentSettings) {
            self.gatherData(function(err, data) {

                var articleService = new pb.ArticleService();
                articleService.getMetaInfo(data.content[0], function(err, meta) {
                    self.ts.registerLocal('navigation', new pb.TemplateValue(data.nav.navigation, false));
                });
            });
        });

        //Load the content for the page
        // Custom Product Sections
        var opts = {
            where: {name: 'Product Sections'},
            limit: 1
        };
        var daoSectionType = new pb.DAO();
        daoSectionType.q('custom_object_type', opts, function(err, sectionObjectType) {
            if (util.isError(err)) {
                return self.serveError(err);
            }
            else if (!util.isObject(sectionObjectType)) {
                return self.reqHandler.serve404();
            }
            var sectionTypeObjectId = self.stringToHex(sectionObjectType[0]._id.id);

            var daoSection = new pb.DAO();
            daoSection.q('custom_object', {where: {type: sectionTypeObjectId, name: sectionName}, limit: 1}, function(err, sectionObject) {
                if (util.isError(err)) {
                    return self.serveError(err);
                }
                if (sectionObject.length == 0) {
                    return self.reqHandler.serve404();
                }
                var sectionObj = pb.ClientJs.getAngularObjects({
                    sectionObject: sectionObject[0]
                });
                self.ts.registerLocal('section_object', new pb.TemplateValue(sectionObj, false));
                self.ts.registerLocal('section_display_name', new pb.TemplateValue(sectionObject[0]['Display Name'], false));

                var categoryObjectResults = [];
                _.each(sectionObject[0].Categories, function(categoryId) {
                    var daoCategory = new pb.DAO();
                    daoCategory.loadById(categoryId, 'custom_object', {}, function (err, categoryObject) {
                        if (util.isError(err)) {
                            return self.reqHandler.serveError(err);
                        }

                        //Update each category object to include their image url's and names
                        var imageUrl = '/public/bl-default-theme/img/placeholders/placeholder.png';
                        var imageName = 'Placeholder';
                        var count = 0;
                        _.each(categoryObject.Media, function (mediaId) {
                            var daoCategoryMedia = new pb.DAO();
                            daoCategoryMedia.loadById(mediaId, 'media', {}, function (err, mediaObject) {
                                count++;
                                if (util.isError(err)) {
                                    return self.reqHandler.serveError(err);
                                }
                                if (mediaObject !== null && mediaObject.media_type == 'image') {
                                    imageUrl = mediaObject.location;
                                    imageName = mediaObject.name;
                                }

                                categoryObject['imageUrl'] = imageUrl;
                                categoryObject['imageName'] = imageName;

                                if(count == categoryObject.Media.length) {
                                    categoryObjectResults.push(categoryObject);

                                    //Register the angular array of category objects
                                    var angularObjects = pb.ClientJs.getAngularObjects({ categoryObjects: categoryObjectResults });
                                    self.ts.registerLocal('category_objects', new pb.TemplateValue(angularObjects, false));
                                }
                            });
                        });
                        categoryObject['imageUrl'] = imageUrl;
                        categoryObject['imageName'] = imageName;

                        if(categoryObject.Media.length == 0) {
                            categoryObjectResults.push(categoryObject);
                        }

                        //Register the angular array of category objects
                        var angularObjects = pb.ClientJs.getAngularObjects({ categoryObjects: categoryObjectResults });
                        self.ts.registerLocal('category_objects', new pb.TemplateValue(angularObjects, false));
                    });
                });

                //Register the angular array of category objects
                var angularObjects = pb.ClientJs.getAngularObjects({ categoryObjects: categoryObjectResults });
                self.ts.registerLocal('category_objects', new pb.TemplateValue(angularObjects, false));

                //Pass through directly to category if there is only one
                if(sectionObject[0].Categories.length == 1) {
                    var daoSingleCategory = new pb.DAO();
                    daoSingleCategory.loadById(sectionObject[0].Categories[0], 'custom_object', {}, function (err, categoryObject) {
                        if (util.isError(err)) {
                            return self.reqHandler.serveError(err);
                        }

                        //redirect
                        self.redirect(sectionName + "/" + categoryObject.name, cb);
                    });
                } else {
                    self.ts.registerLocal('page_name', new pb.TemplateValue(sectionName + ' | ' + config.siteName, false));

                    self.ts.load('categories', function(err, result) {
                        if (util.isError(err)) {
                            return cb(err);
                        }
                        cb({content: result});
                    });
                }
            });
        });
    };

    CategoryObjects.prototype.gatherData = function(cb) {
        var self  = this;
        var tasks = {

            //navigation
            nav: function(callback) {
                self.getNavigation(function(themeSettings, navigation, accountButtons) {
                    callback(null, {themeSettings: themeSettings, navigation: navigation, accountButtons: accountButtons});
                });
            },

            //articles, pages, etc.
            content: function(callback) {
                self.loadContent(callback);
            },

            section: function(callback) {
                if(!self.req.pencilblue_section) {
                    callback(null, {});
                    return;
                }

                var dao = new pb.DAO();
                dao.loadById(self.req.pencilblue_section, {}, 'section', callback);
            }
        };
        async.parallel(tasks, cb);
    };

    CategoryObjects.prototype.loadContent = function(articleCallback) {

        var section = this.req.pencilblue_section || null;
        var topic   = this.req.pencilblue_topic   || null;
        var article = this.req.pencilblue_article || null;
        var page    = this.req.pencilblue_page    || null;

        //get service context
        var opts = this.getServiceContext();

        var service = new ArticleService();
        if(this.req.pencilblue_preview) {
            if(this.req.pencilblue_preview == page || article) {
                if(page) {
                    service.setContentType('page');
                }
                var where = pb.DAO.getIdWhere(page || article);
                where.draft = {$exists: true};
                where.publish_date = {$exists: true};
                service.find(where, opts, articleCallback);
            }
            else {
                service.find({}, opts, articleCallback);
            }
        }
        else if(section) {
            service.findBySection(section, articleCallback);
        }
        else if(topic) {
            service.findByTopic(topic, articleCallback);
        }
        else if(article) {
            service.findById(article, articleCallback);
        }
        else if(page) {
            service.setContentType('page');
            service.findById(page, articleCallback);
        }
        else{
            service.find({}, opts, articleCallback);
        }
    };

    CategoryObjects.prototype.getNavigation = function(cb) {
        var options = {
            currUrl: this.req.url,
            session: this.session,
            ls: this.ls,
            activeTheme: this.activeTheme
        };

        var menuService = new pb.TopMenuService();
        menuService.getNavItems(options, function(err, navItems) {
            if (util.isError(err)) {
                pb.log.error('CategoryObjects: %s', err.stack);
            }
            cb(navItems.themeSettings, navItems.navigation, navItems.accountButtons);
        });
    };

    CategoryObjects.prototype.stringToHex = function (tmp) {
        var str = '',
            i = 0,
            tmp_len = tmp.length,
            c;

        for (; i < tmp_len; i += 1) {
            c = tmp.charCodeAt(i);
            str += this.d2h(c);
        }
        return str;
    };
    CategoryObjects.prototype.d2h = function(number) {
        if (number < 0) {
            number = 0xFFFFFFFF + number + 1;
        }
        return this.pad(number.toString(16), 2);
    };
    CategoryObjects.prototype.pad = function(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    };

    /**
     * Provides the routes that are to be handled by an instance of this prototype.
     * The route provides a definition of path, permissions, authentication, and
     * expected content type.
     * Method is optional
     * Path is required
     * Permissions are optional
     * Access levels are optional
     * Content type is optional
     *
     * @param cb A callback of the form: cb(error, array of objects)
     */
    CategoryObjects.getRoutes = function(cb) {
        var routes = [
            {
                method: 'get',
                path: '/page/products/:section_name',
                auth_required: false,
                content_type: 'text/html'
            }
        ];
        cb(null, routes);
    };

    //exports
    return CategoryObjects;
};