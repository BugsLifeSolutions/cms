/**
 * Created by HadenHiles on 15-09-11.
 */

module.exports = function BannersModule(pb) {

    /**
     * Banners - A Banners plugin for PencilBlue
     *
     * @author Blake Callens <blake@pencilblue.org>
     * @copyright 2014 PencilBlue, LLC
     */
    function Banners(){}

    /**
     * Called when the application is being installed for the first time.
     *
     * @param cb A callback that must be called upon completion.  cb(err, result).
     * The result is ignored
     */
    Banners.onInstallWithContext = function(context, cb) {
        var self = this;

        var cos = new pb.CustomObjectService();
        cos.loadTypeByName('Banners', function(err, bannerType) {
            if (!bannerType) {
                var bannerValues = {
                    name: 'Banners',
                    fields: {
                        name: {
                            field_type: 'text'
                        },
                        title: {
                            field_type: 'text'
                        },
                        caption: {
                            field_type: 'text'
                        },
                        link: {
                            field_type: 'text'
                        },
                        action: {
                            field_type: 'text'
                        },
                        online: {
                            field_type: 'boolean'
                        },
                        order: {
                            field_type: 'number'
                        },
                        image: {
                            field_type: 'peer_object',
                            object_type: 'media'
                        }
                    }
                };

                cos.saveType(bannerValues, function (err, bannerType) {
                    cb(err, true);
                });
            } else {
                cb(null, true);
            }
        });
    };

    /**
     * Called when the application is uninstalling this plugin.  The plugin should
     * make every effort to clean up any plugin-specific DB items or any in function
     * overrides it makes.
     *
     * @param context
     * @param cb A callback that must be called upon completion.  cb(Error, Boolean).
     * The result should be TRUE on success and FALSE on failure
     */
    Banners.onUninstallWithContext = function (context, cb) {
        var site = pb.SiteService.getCurrentSite(context.site);

        // Remove "banners" nav during uninstall
        pb.AdminNavigation.removeFromSite("banners", site);
        cb(null, true);
    };

    /**
     * Called when the application is starting up. The function is also called at
     * the end of a successful install. It is guaranteed that all core PB services
     * will be available including access to the core DB.
     *
     * @param context
     * @param cb A callback that must be called upon completion.  cb(Error, Boolean).
     * The result should be TRUE on success and FALSE on failure
     */
    Banners.onStartupWithContext = function (context, cb) {
        var self = this;
        pb.TemplateService.registerGlobal('banner_height', function(flag, cb) {
            var pluginService = new pb.PluginService();
            pluginService.getSetting('height', 'bl-banners', cb);
        });

        //Get the type ID
        var typeID = "";
        var cos = new pb.CustomObjectService();
        cos.loadTypeByName('Banners', function(err, objectType) {
            if(err == null && objectType) {
                typeID = self.stringToHex(objectType._id.id);

                /**
                 * Administration Navigation sample
                 */
                var site = pb.SiteService.getCurrentSite(context.site);

                // Add a child to the top level node "content"
                pb.AdminNavigation.addChildToSite("content", {
                    id: "banners",
                    title: "Banners",
                    icon: "photo",
                    href: "/admin/content/objects/" + typeID,
                    access: pb.SecurityService.ACCESS_USER
                }, site);
            }

            cb(null, true);
        });
    };

    /**
     * Called when the application is gracefully shutting down.  No guarantees are
     * provided for how much time will be provided the plugin to shut down.
     *
     * @param cb A callback that must be called upon completion.  cb(err, result).
     * The result is ignored
     */
    Banners.onShutdown = function(cb) {
        cb(null, true);
    };

    Banners.stringToHex = function (tmp) {
        var str = '',
            i = 0,
            tmp_len = tmp.length,
            c;

        for (; i < tmp_len; i += 1) {
            c = tmp.charCodeAt(i);
            str += this.d2h(c);
        }
        return str;
    };
    Banners.d2h = function(number) {
        if (number < 0) {
            number = 0xFFFFFFFF + number + 1;
        }
        return this.pad(number.toString(16), 2);
    };
    Banners.pad = function(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    };

    //exports
    return Banners;
};
